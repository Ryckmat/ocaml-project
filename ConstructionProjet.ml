type element = E | F | A | T | N | I;;
type rang = P | M | U;;
type personne = {nom : string ; ele : element; ran : rang};;
  
type arbre =
    | Feuille
    | Noeud of personne * arbre * arbre;;
let aff_ele = function                                        
| E -> "eau"
| F -> "feu"
| A -> "air"
| T -> "terre"
| N -> "aucun"
| I -> "?";;
let aff_perso_no_rang p =
  p.nom ^ "[" ^ aff_ele p.ele ^ "]";;
let rec aff_arbre_no_rang = function
| Feuille -> ""
| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> aff_perso_no_rang p1 ^ "(" ^ aff_arbre_no_rang (Noeud(a,b,c)) ^
  " , " ^ aff_arbre_no_rang (Noeud(d,e,f)) ^ ")"
| Noeud (p1,Noeud(g,h,i),Feuille) -> aff_perso_no_rang p1 ^ "(" ^ aff_arbre_no_rang (Noeud(g,h,i)) ^
  " , "
| Noeud (p1,Feuille,Noeud(k,l,m)) -> aff_perso_no_rang p1 ^ aff_arbre_no_rang (Noeud(k,l,m)) ^
  ")"
| Noeud (p1,Feuille,Feuille) -> aff_perso_no_rang p1;;


let aff_arbre_no_rang_complet abr =
  print_string (aff_arbre_no_rang abr);;


let p1 = {nom = "a"; ele = E ; ran = U};;
let p2 = {nom = "b"; ele = N ; ran = U};;
let p3 = {nom = "c"; ele = A ; ran = U};;
let p4 = {nom = "d"; ele = E ; ran = U};;
let p5 = {nom = "e"; ele = F ; ran = U};;
let p6 = {nom = "h"; ele = T ; ran = U};;
let p7 = {nom = "i"; ele = I ; ran = U};;



let f = Feuille;;
let a = Noeud(p6,f,f);;
let b = Noeud(p7,f,f);;
let c = Noeud(p3,f,f);;
let d = Noeud(p4,f,f);;

let arbre = Noeud(p1,Noeud(p2, d,Noeud(p5,a, b)), c);;
aff_arbre_no_rang_complet arbre;;

let rec compteur_elem el abr = match abr with
| Feuille -> 0
| Noeud (p1,a1,a2) -> if p1.ele = el then 1 + (compteur_elem el a1) 
					  + (compteur_elem el a2)
					  else (compteur_elem el a1) + (compteur_elem el a2);; 

compteur_elem E arbre;;

compteur_elem F arbre;;

let rec empilement abr l = match abr with
| Feuille -> l
| Noeud (p1,a1,a2) -> p1 :: empilement a1 l @ empilement a2 l;;

let rec recherche_elem_perso n= function
[] -> I
|p::l -> if n=p.nom then p.ele else recherche_elem_perso n l;;

recherche_elem_perso "e" (empilement arbre []);;
recherche_elem_perso "z" (empilement arbre []);;


let p1 = {nom = "a"; ele = E ; ran = U};;
let p2 = {nom = "b"; ele = I ; ran = U};;
let p3 = {nom = "c"; ele = I ; ran = U};;
let p4 = {nom = "d"; ele = E ; ran = U};;
let p5 = {nom = "e"; ele = I ; ran = U};;
let p8 = {nom = "f"; ele = A ; ran = U};;
let p9 = {nom = "g"; ele = A ; ran = U};;
let p6 = {nom = "h"; ele = T ; ran = U};;
let p7 = {nom = "i"; ele = F ; ran = U};;

let a = Noeud(p4,f,f);;
let b = Noeud(p6,f,f);;
let c = Noeud(p7,f,f);;
let d = Noeud(p8,f,f);;
let e = Noeud(p9,f,f);;


let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

let comparaison_PM modif mere pere = match (mere.ele, pere.ele) with
| (F ,E) -> {nom=modif.nom;ele=N;ran=U}
| (E ,F) -> {nom=modif.nom;ele=N;ran=U}
| (A ,T) -> {nom=modif.nom;ele=N;ran=U}
| (T ,A) -> {nom=modif.nom;ele=N;ran=U}
| (T ,T) -> {nom=modif.nom;ele=T;ran=U}
| (A ,A) -> {nom=modif.nom;ele=A;ran=U}
| (F ,_) -> {nom=modif.nom;ele=F;ran=U}
| (_ ,F) -> {nom=modif.nom;ele=F;ran=U}
| (N ,_) -> {nom=modif.nom;ele=E;ran=U}
| (_ ,N) -> {nom=modif.nom;ele=E;ran=U}
| (E ,_) -> {nom=modif.nom;ele=E;ran=U}
| (_ ,E) -> {nom=modif.nom;ele=E;ran=U}
| (_ ,_) -> {nom=modif.nom;ele=I;ran=U};;

let  complet abr = match abr with
| Feuille -> f
| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> Noeud(comparaison_PM p1 a d, Noeud(a,b,c), Noeud(d,e,f)) 
| Noeud (p2,a2,b2) -> Noeud(p2,a2,b2);;

 let rec complet2 abr = match abr with
| Feuille -> f
| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) ->	if (a.ele = I) then Noeud(p1,complet (complet2 (Noeud(a,b,c))), complet2 (Noeud(d,e,f)) )
											else if (d.ele = I) then Noeud(p1, complet2 (Noeud(a,b,c)), complet (complet2 (Noeud(d,e,f))) )
											else if (p1.ele =I) then complet (Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)))
											else Noeud(p1, complet2 (Noeud(a,b,c)), complet2 (Noeud(d,e,f)))
 | Noeud (p2,a2,b2) -> Noeud(p2,a2,b2);;

let arbre2 = complet2 arbre;;

let possiblite_elem elem = match elem with 
| E-> [E;T;A]
| F-> [F;T;A]
| T-> [T]
| A-> [A]
| N-> [E;T;A;F]
| I-> [];;

let rec contains elem l = match l with 
| [] -> false
| a::l -> if a=elem then true else contains elem l;;



let verification fils mere pere = match (fils.ele) with
|E-> contains mere.ele (possiblite_elem mere.ele) || contains pere.ele (possiblite_elem pere.ele)
|F-> contains mere.ele (possiblite_elem mere.ele) || contains pere.ele (possiblite_elem pere.ele)
|A-> contains mere.ele (possiblite_elem mere.ele) && contains pere.ele (possiblite_elem pere.ele)
|T-> contains mere.ele (possiblite_elem mere.ele) && contains pere.ele (possiblite_elem pere.ele)
|N-> mere.ele = F && pere.ele =E || mere.ele = E && pere.ele =F || mere.ele = T && pere.ele = A || mere.ele = A && pere.ele =T  
|I-> false;;

let rec empilement_Noeud abr l = match abr with
| Feuille -> l
| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> (p1,a,d) :: empilement_Noeud (Noeud(a,b,c)) l @ empilement_Noeud (Noeud(d,e,f)) l
| Noeud (p,a,b) -> l;;


let rec verif_arbre = function
[] -> true
| (a,b,c)::l -> if (verification a b c) then true && verif_arbre l else false;;

let p1 = {nom = "a"; ele = E ; ran = U};;
let p2 = {nom = "b"; ele = N ; ran = U};;
let p3 = {nom = "c"; ele = A ; ran = U};;
let p4 = {nom = "d"; ele = E ; ran = U};;
let p5 = {nom = "e"; ele = E ; ran = U};;
let p8 = {nom = "f"; ele = A ; ran = U};;
let p9 = {nom = "g"; ele = A ; ran = U};;
let p6 = {nom = "h"; ele = T ; ran = U};;
let p7 = {nom = "i"; ele = F ; ran = U};;

let a = Noeud(p4,f,f);;
let b = Noeud(p6,f,f);;
let c = Noeud(p7,f,f);;
let d = Noeud(p8,f,f);;
let e = Noeud(p9,f,f);;


let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;
let list2 = empilement_Noeud arbre [];;
verif_arbre list2;;

let list = empilement_Noeud arbre2 [];;
verif_arbre list;;


let rec aff_ele_et_rang ran elem = match (ran, elem) with                                      
| (M,_) -> aff_ele elem
| (P,F) -> "feu+lave"
| (P,E) -> "eau+glace"
| (P,T) -> "terre+bois"
| (P,A) -> "aire+foudre"
| (P,N) -> "aucun"
| (U,I) -> "??"
| (_,I) -> "?+"
| (U,_) -> aff_ele_et_rang P elem ^ "?";;

let aff_perso_avec_rang p =
  p.nom ^ "[" ^ aff_ele_et_rang p.ran p.ele ^ "]";;
let rec aff_arbre_with_rang = function
| Feuille -> ""
| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> aff_perso_avec_rang p1 ^ "(" ^ aff_arbre_with_rang (Noeud(a,b,c)) ^
  " , " ^ aff_arbre_with_rang (Noeud(d,e,f)) ^ ")"
| Noeud (p1,Noeud(g,h,i),Feuille) -> aff_perso_avec_rang p1 ^ "(" ^ aff_arbre_with_rang (Noeud(g,h,i)) ^
  " , "
| Noeud (p1,Feuille,Noeud(k,l,m)) -> aff_perso_avec_rang p1 ^ aff_arbre_with_rang (Noeud(k,l,m)) ^
  ")"
| Noeud (p1,Feuille,Feuille) -> aff_perso_avec_rang p1;;

let p1 = {nom = "a"; ele = I ; ran = P};;
let p2 = {nom = "b"; ele = N ; ran = P};;
let p3 = {nom = "c"; ele = A ; ran = M};;
let p4 = {nom = "d"; ele = E ; ran = U};;
let p5 = {nom = "e"; ele = I ; ran = U};;
let p6 = {nom = "h"; ele = T ; ran = P};;
let p7 = {nom = "i"; ele = I ; ran = M};;



let f = Feuille;;
let a = Noeud(p6,f,f);;
let b = Noeud(p7,f,f);;
let c = Noeud(p3,f,f);;
let d = Noeud(p4,f,f);;

let arbre3 = Noeud(p1,Noeud(p2, d,Noeud(p5,a, b)), c);;

aff_arbre_with_rang arbre3;;

let verification_rang fils mere pere = match (fils.ran,mere.ran,pere.ran) with
| (_, U, _) |(_,_,U) | (U,_,_)  -> true 
| (P,_,_) -> true
| (M,M,M) -> true
| (_,_,_) -> false;;

let rec verif_arbre_rang = function
[] -> true
| (a,b,c)::l -> if (verification_rang a b c) then true && verif_arbre_rang l else false;;

let p1 = {nom = "a"; ele = E ; ran = P};;
let p2 = {nom = "b"; ele = N ; ran = P};;
let p3 = {nom = "c"; ele = A ; ran = P};;
let p4 = {nom = "d"; ele = E ; ran = M};;
let p5 = {nom = "e"; ele = E ; ran = M};;
let p8 = {nom = "f"; ele = A ; ran = M};;
let p9 = {nom = "g"; ele = A ; ran = P};;
let p6 = {nom = "h"; ele = T ; ran = P};;
let p7 = {nom = "i"; ele = F ; ran = P};;

let a = Noeud(p4,f,f);;
let b = Noeud(p6,f,f);;
let c = Noeud(p7,f,f);;
let d = Noeud(p8,f,f);;
let e = Noeud(p9,f,f);;


let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

let list1 = empilement_Noeud arbre [];;
verif_arbre_rang list1;;

let p1 = {nom = "a"; ele = E ; ran = P};;
let p2 = {nom = "b"; ele = N ; ran = P};;
let p3 = {nom = "c"; ele = A ; ran = P};;
let p4 = {nom = "d"; ele = E ; ran = M};;
let p5 = {nom = "e"; ele = E ; ran = P};;
let p8 = {nom = "f"; ele = A ; ran = M};;
let p9 = {nom = "g"; ele = A ; ran = P};;
let p6 = {nom = "h"; ele = T ; ran = P};;
let p7 = {nom = "i"; ele = F ; ran = P};;

let a = Noeud(p4,f,f);;
let b = Noeud(p6,f,f);;
let c = Noeud(p7,f,f);;
let d = Noeud(p8,f,f);;
let e = Noeud(p9,f,f);;


let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;
let list2 = empilement_Noeud arbre [];;
verif_arbre_rang list2;;


let rec hauteur = function
| Feuille -> 0
| Noeud(_,g,d) -> 1 + (max (hauteur g) (hauteur d)) ;;

let rec par_niveau t l = match t with
| Feuille -> []
| Noeud(c, a, b) ->
    if l = 1 then [c]
    else par_niveau a (l - 1) @ par_niveau b (l - 1);;

let rec parcours_en_largeur arbre haut l =
	if haut = 0 then l
	else parcours_en_largeur arbre (haut-1) ((par_niveau arbre haut) :: l) ;;

let li =parcours_en_largeur arbre hau [];;

let lili = List.flatten li;;

let rec renverse l1 l2 = match l2 with
[] -> l1
| a :: tl -> renverse (a :: l1) tl;;

let rec recherche_ancetre n= function
[] -> ""
|p::l -> if n=p.ele then p.nom else recherche_ancetre n l;;

let p1 = {nom = "a"; ele = E ; ran = P};;
let p2 = {nom = "b"; ele = N ; ran = P};;
let p3 = {nom = "c"; ele = A ; ran = P};;
let p4 = {nom = "d"; ele = E ; ran = M};;
let p5 = {nom = "e"; ele = E ; ran = P};;
let p8 = {nom = "f"; ele = A ; ran = M};;
let p9 = {nom = "g"; ele = A ; ran = P};;
let p6 = {nom = "h"; ele = T ; ran = P};;
let p7 = {nom = "i"; ele = F ; ran = P};;

let a = Noeud(p4,f,f);;
let b = Noeud(p6,f,f);;
let c = Noeud(p7,f,f);;
let d = Noeud(p8,f,f);;
let e = Noeud(p9,f,f);;


let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

let hau = hauteur arbre;;
let li =parcours_en_largeur arbre hau [];;

let lili = List.flatten li;;

let list_ancetre = renverse [] lili;;
recherche_ancetre E list_ancetre;;
recherche_ancetre I list_ancetre;;
recherche_ancetre F list_ancetre;;
recherche_ancetre N list_ancetre;;


let miroir a = match a with
Feuille -> Feuille
| Noeud(e,x,y)-> Noeud(e, y , x);;


let supp a = match a with 
Feuille -> Feuille
| Noeud (e,x,y) -> if e.ele = N then Noeud(e,f,f) else Noeud(e, x, y);;


let modif_elem a = match  a with
| Feuille -> Feuille
| Noeud(e,x,y) -> if e.ele = A then Noeud({nom = e.nom ; ele = F ; ran = e.ran}, x, y) else  Noeud(e, x, y);;

let modif_rang a = match a with 
| Feuille -> Feuille
| Noeud(e,x,y) -> if e.ran = P then Noeud({nom = e.nom ; ele = e.ele; ran = M}, x, y) else  Noeud({nom = e.nom ; ele = e.ele; ran = P}, x, y);;

let rec ordre_sup f1 f2 a = match a with
  | Feuille -> Feuille
  | Noeud(e,x,y) -> f2 (f1 (Noeud(e, ordre_sup f2 f1 x , ordre_sup f2 f1 y)));;

let p1 = {nom = "a"; ele = E ; ran = P};;
let p2 = {nom = "b"; ele = N ; ran = P};;
let p3 = {nom = "c"; ele = A ; ran = P};;
let p4 = {nom = "d"; ele = E ; ran = M};;
let p5 = {nom = "e"; ele = E ; ran = P};;
let p8 = {nom = "f"; ele = A ; ran = M};;
let p9 = {nom = "g"; ele = A ; ran = P};;
let p6 = {nom = "h"; ele = T ; ran = P};;
let p7 = {nom = "i"; ele = F ; ran = P};;

let a = Noeud(p4,f,f);;
let b = Noeud(p6,f,f);;
let c = Noeud(p7,f,f);;
let d = Noeud(p8,f,f);;
let e = Noeud(p9,f,f);;


let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

ordre_sup miroir supp arbre;;

ordre_sup modif_rang modif_elem arbre;;