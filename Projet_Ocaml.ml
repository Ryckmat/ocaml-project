(*Projet Ocaml : Ryckman Matthieu Gr 2.2*)

(*Partie 1: Définissez le(s) type(s) nécessaire(s) à la représentation d’un arbre 
	généalogique et illustrez-le(s) à l’aide de plusieurs exemples de valeurs de 
	ce(s) type(s).
*)

(*Le but de cette partie est réalisé un arbre généalogique selon les types 
	adéquates. Pour la structure de l'arbre nous allons sur un arbre binaire
	composé de noeud et feuille. Pour le typage d'une personne elle sera
	composé du nom (a,b,c...), d'un élement( E,A,F,T,N (non-élementaire) et I 
	pour inconue ),et du rang (+(noté dans le code H), =(S) et U pour incounnue)
*)

type element = E | F | A | T | N | I;;
type rang = P | M | U;;
type personne = {nom : string; ele : element;ran : rang};;
  
type arbre =
    | Feuille
    | Noeud of personne * arbre * arbre;;

(*	Exemple de la construction dde l'arbre:
	type element = E | F | A | T | N | I;;
	type element = E | F | A | T | N | I
	type rang = P | M | U;;
	type rang = P | M | U
	type personne = {nom : string ; ele : element; ran : rang};;
	type personne = { nom : string; ele : element; ran : rang; }
	  
	type arbre =
	    | Feuille
	    | Noeud of personne * arbre * arbre;;
	type arbre = Feuille | Noeud of personne * arbre * arbre
	let p1 = {nom = "a"; ele = E ; ran = U};;
	val p1 : personne = {nom = "a"; ele = E; ran = U}
	let p2 = {nom = "b"; ele = N ; ran = U};;
	val p2 : personne = {nom = "b"; ele = N; ran = U}
	let p3 = {nom = "c"; ele = A ; ran = U};;
	val p3 : personne = {nom = "c"; ele = A; ran = U}
	let p4 = {nom = "d"; ele = E ; ran = U};;
	val p4 : personne = {nom = "d"; ele = E; ran = U}
	let p5 = {nom = "e"; ele = F ; ran = U};;
	val p5 : personne = {nom = "e"; ele = F; ran = U}
	let p6 = {nom = "h"; ele = T ; ran = U};;
	val p6 : personne = {nom = "h"; ele = T; ran = U}
	let p7 = {nom = "i"; ele = I ; ran = U};;
	val p7 : personne = {nom = "i"; ele = I; ran = U}
	# 
	  
	  
	let f = Feuille;;
	val f : arbre = Feuille
	let a = Noeud(p6,f,f);;
	val a : arbre = Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille)
	let b = Noeud(p7,f,f);;
	val b : arbre = Noeud ({nom = "i"; ele = I; ran = U}, Feuille, Feuille)
	let c = Noeud(p3,f,f);;
	val c : arbre = Noeud ({nom = "c"; ele = A; ran = U}, Feuille, Feuille)
	let d = Noeud(p4,f,f);;
	val d : arbre = Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille)
	# 
	  let arbre = Noeud(p1,Noeud(p2, d,Noeud(p5,a, b)), c);;
	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = U},
	   Noeud ({nom = "b"; ele = N; ran = U},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = F; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = I; ran = U}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = U}, Feuille, Feuille))

*)

(*----------------------------------------------------------------------------*)

(*Partie 2: Ecrivez la (les) fonction(s) nécessaire(s) pour afficher un arbre 
	généalogique comme décrit ci-dessus. Veillez à ne faire qu’un unique appel 
	non fonctionnel. Illustrez l’affichage à l’aide d’un appel sur un exemple 
	significatif et mettez en commentaire le résultat de l’évaluation OCaml.
*)

(*Dans un premier tamps nous allons concevoir une fonction qui permet 
	d'afficher l'element de la personne. Ensuite une fonction récursive principal 
	pour l'affichage de l'enssemble de l'arbre en donnant la priorité à la 
	mère. Dans cette partie on affichera pas le rang on definira une autre 
	méthode dans les parties suivantes.
*)

let aff_ele = function                                        
| E -> "eau"
| F -> "feu"
| A -> "air"
| T -> "terre"
| N -> "aucun"
| I -> "?";;

let aff_perso_no_rang p =
  p.nom ^ "[" ^ aff_ele p.ele ^ "]";;

let rec aff_arbre_no_rang = function
| Feuille -> ""
| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> aff_perso_no_rang p1 ^ "(" ^ aff_arbre_no_rang (Noeud(a,b,c)) ^
  " , " ^ aff_arbre_no_rang (Noeud(d,e,f)) ^ ")"
| Noeud (p1,Noeud(g,h,i),Feuille) -> aff_perso_no_rang p1 ^ "(" ^ aff_arbre_no_rang (Noeud(g,h,i)) ^
  " , "
| Noeud (p1,Feuille,Noeud(k,l,m)) -> aff_perso_no_rang p1 ^ aff_arbre_no_rang (Noeud(k,l,m)) ^
  ")"
| Noeud (p1,Feuille,Feuille) -> aff_perso_no_rang p1;;


let aff_arbre_no_rang_complet abr =
  print_string (aff_arbre_no_rang abr);;

(* Exemple realisation de la figure 3 du projet:
	type element = E | F | A | T | N | I;;
	type element = E | F | A | T | N | I
	type rang = P | M | U;;
	type rang = P | M | U
	type personne = {nom : string; mutable ele : element; mutable ran : rang};;
	type personne = { nom : string; mutable ele : element; mutable ran : rang; }
	  
	type arbre =
	    | Feuille
	    | Noeud of personne * arbre * arbre;;
	type arbre = Feuille | Noeud of personne * arbre * arbre
	let aff_ele = function                                        
	| E -> "eau"
	| F -> "feu"
	| A -> "air"
	| T -> "terre"
	| N -> "aucun"
	| I -> "?";;
	val aff_ele : element -> string = <fun>
	let aff_perso_no_rang p =
	  p.nom ^ "[" ^ aff_ele p.ele ^ "]";;
	val aff_perso_no_rang : personne -> string = <fun>
	let rec aff_arbre_no_rang = function
	| Feuille -> ""
	| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> aff_perso_no_rang p1 ^ "(" ^ aff_arbre_no_rang (Noeud(a,b,c)) ^
	  " , " ^ aff_arbre_no_rang (Noeud(d,e,f)) ^ ")"
	| Noeud (p1,Noeud(g,h,i),Feuille) -> aff_perso_no_rang p1 ^ "(" ^ aff_arbre_no_rang (Noeud(g,h,i)) ^
	  " , "
	| Noeud (p1,Feuille,Noeud(k,l,m)) -> aff_perso_no_rang p1 ^ aff_arbre_no_rang (Noeud(k,l,m)) ^
	  ")"
	| Noeud (p1,Feuille,Feuille) -> aff_perso_no_rang p1;;

	val aff_arbre_no_rang : arbre -> string = <fun>
	#   
	let aff_arbre_no_rang_complet abr =
	  print_string (aff_arbre_no_rang abr);;
	val aff_arbre_no_rang_complet : arbre -> unit = <fun>
	# 
	  
	let p1 = {nom = "a"; ele = E ; ran = U};;
	val p1 : personne = {nom = "a"; ele = E; ran = U}
	let p2 = {nom = "b"; ele = N ; ran = U};;
	val p2 : personne = {nom = "b"; ele = N; ran = U}
	let p3 = {nom = "c"; ele = A ; ran = U};;
	val p3 : personne = {nom = "c"; ele = A; ran = U}
	let p4 = {nom = "d"; ele = E ; ran = U};;
	val p4 : personne = {nom = "d"; ele = E; ran = U}
	let p5 = {nom = "e"; ele = F ; ran = U};;
	val p5 : personne = {nom = "e"; ele = F; ran = U}
	let p6 = {nom = "h"; ele = T ; ran = U};;
	val p6 : personne = {nom = "h"; ele = T; ran = U}
	let p7 = {nom = "i"; ele = I ; ran = U};;
	val p7 : personne = {nom = "i"; ele = I; ran = U}
	# 
	  
	  
	let f = Feuille;;
	val f : arbre = Feuille
	let a = Noeud(p6,f,f);;
	val a : arbre = Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille)
	let b = Noeud(p7,f,f);;
	val b : arbre = Noeud ({nom = "i"; ele = I; ran = U}, Feuille, Feuille)
	let c = Noeud(p3,f,f);;
	val c : arbre = Noeud ({nom = "c"; ele = A; ran = U}, Feuille, Feuille)
	let d = Noeud(p4,f,f);;
	val d : arbre = Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille)
	# 
	let arbre = Noeud(p1,Noeud(p2, d,Noeud(p5,a, b)), c);;
	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = U},
	   Noeud ({nom = "b"; ele = N; ran = U},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = F; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = I; ran = U}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = U}, Feuille, Feuille))
	# aff_arbre_no_rang_complet arbre;;
	a[eau](b[aucun](d[eau] , e[feu](h[terre] , i[?])) , c[air])- : unit = ()
*)

(*Partie 3 : Ecrivez les fonctions nécessaires pour calculer le nombre 
	d’individus d’un pouvoir donné dans un arbre. Illustrez ce calcul à l’aide 
	d’un appel sur un exemple significatif et mettez en commentaire le résultat 
	de  l’évaluation OCaml. Ecrivez les fonctions nécessaires pour retrouver le 
	pouvoir  d’un individu donné dans un arbre. Illustrez ce calcul à l’aide 
	d’un appel sur un exemple significatif et mettez en commentaire le résultat 
	de l’évaluation OCaml.	
*)

(*Dans cette partie on va utiliser des fonctions recursives. Pour le compteur
	d'élément si on tombe sur une feuille on renvoie 0 car les feuilles ne continnent pas d'information
	.Si on tombe sur un noeud on teste la personne qui lie le noeud on
	incrémente si les élément corresponde et apres on effectue la récursion sur 
	les branches de l'arbre.
*)

let rec compteur_elem el abr = match abr with
| Feuille -> 0
| Noeud (p1,a1,a2) -> if p1.ele = el then 1 + (compteur_elem el a1) 
					  + (compteur_elem el a2)
					  else (compteur_elem el a1) + (compteur_elem el a2);;

(* A partir de cette question nous nous baserons sur l'arbre ci dessous :
	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = U},
	   Noeud ({nom = "b"; ele = N; ran = U},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = F; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = I; ran = U}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = U}, Feuille, Feuille))
	# aff_arbre_no_rang_complet arbre;;

	let rec compteur_elem el abr = match abr with
	| Feuille -> 0
	| Noeud (p1,a1,a2) -> if p1.ele = el then 1 + (compteur_elem el a1) 
	  + (compteur_elem el a2)
	  else (compteur_elem el a1) + (compteur_elem el a2);; 
	val compteur_elem : element -> arbre -> int = <fun>
	# 
	compteur_elem E arbre;;
	- : int = 2
	# 
	  compteur_elem F arbre;;
	- : int = 1
*)

(*Pour la partie pour retrouver le pouvoir d'un individue on va utiliser aussi
	une fonction récursive on parcours l'arbre tant qu'il y a une structure 
	un noeud que l'on rajoute au fure et a meusure dans une liste
	.Quand l'arbre a été fini d'etre parcourus on cherche dans la liste la 
	personne en question si elle y est on retourne son élément sinon on retourne 
	une élément inconnue.
*)

let rec empilement abr l = match abr with
| Feuille -> l
| Noeud (p1,a1,a2) -> p1 :: empilement a1 l @ empilement a2 l;;

let rec recherche_elem_perso n= function
[] -> I
|p::l -> if n=p.nom then p.ele else recherche_elem_perso n l;;

(*Exemple realisation sur la figure 3 du projet:
	let rec empilement abr l = match abr with
	| Feuille -> l
	| Noeud (p1,a1,a2) -> p1 :: empilement a1 l @ empilement a2 l;;
	val empilement : arbre -> personne list -> personne list = <fun>
	# 
	let rec recherche_elem_perso n= function
	[] -> I
	  |p::l -> if n=p.nom then p.ele else recherche_elem_perso n l;;
	val recherche_elem_perso : string -> personne list -> element = <fun>
	recherche_elem_perso "e" (empilement arbre []);;
	- : element = F
	# recherche_elem_perso "z" (empilement arbre []);;
	- : element = I
	# 
*)

(*	Partie 4 : Ecrivez la (les) fonction(s) nécessaire(s) qui renvoie un arbre généalogique complété avec
	ces règles simplifiées. Illustrez ce calcul à l’aide d’un appel sur un exemple significatif et mettez
	en commentaire le résultat de l’évaluation OCaml.
*)

(* Pour cette partie on a utilisé plusieurs fonctions pour pouvoir compléter l'arbre:
	La première comparaison_PM  permet d'effectuer une comparaison les éléments du père et de la mère 
	et renvoie le fils adéquate selon les regles attribué.
	La seconde complet fonction qui permet d'effectué la comparaison sur un noeud.
	La troisième complet2 permet de compléter l'arbre de façons recursive.
*)

let comparaison_PM modif mere pere = match (mere.ele, pere.ele) with
| (F ,E) -> {nom=modif.nom;ele=N;ran=U}
| (E ,F) -> {nom=modif.nom;ele=N;ran=U}
| (A ,T) -> {nom=modif.nom;ele=N;ran=U}
| (T ,A) -> {nom=modif.nom;ele=N;ran=U}
| (T ,T) -> {nom=modif.nom;ele=T;ran=U}
| (A ,A) -> {nom=modif.nom;ele=A;ran=U}
| (F ,_) -> {nom=modif.nom;ele=F;ran=U}
| (_ ,F) -> {nom=modif.nom;ele=F;ran=U}
| (N ,_) -> {nom=modif.nom;ele=E;ran=U}
| (_ ,N) -> {nom=modif.nom;ele=E;ran=U}
| (E ,_) -> {nom=modif.nom;ele=E;ran=U}
| (_ ,E) -> {nom=modif.nom;ele=E;ran=U}
| (_ ,_) -> {nom=modif.nom;ele=I;ran=U};;

let  complet abr = match abr with
| Feuille -> f
| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> Noeud(comparaison_PM p1 a d, Noeud(a,b,c), Noeud(d,e,f)) 
| Noeud (p2,a2,b2) -> Noeud(p2,a2,b2);;

 let rec complet2 abr = match abr with
| Feuille -> f
| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) ->	if (a.ele = I) then Noeud(p1,complet (complet2 (Noeud(a,b,c))), complet2 (Noeud(d,e,f)) )
											else if (d.ele = I) then Noeud(p1, complet2 (Noeud(a,b,c)), complet (complet2 (Noeud(d,e,f))) )
											else if (p1.ele =I) then complet (Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)))
											else Noeud(p1, complet2 (Noeud(a,b,c)), complet2 (Noeud(d,e,f)))
 | Noeud (p2,a2,b2) -> Noeud(p2,a2,b2);;

(*	Resultat obtenue :
	let p1 = {nom = "a"; ele = E ; ran = U};;
	val p1 : personne = {nom = "a"; ele = E; ran = U}
	let p2 = {nom = "b"; ele = I ; ran = U};;
	val p2 : personne = {nom = "b"; ele = I; ran = U}
	let p3 = {nom = "c"; ele = I ; ran = U};;
	val p3 : personne = {nom = "c"; ele = I; ran = U}
	let p4 = {nom = "d"; ele = E ; ran = U};;
	val p4 : personne = {nom = "d"; ele = E; ran = U}
	let p5 = {nom = "e"; ele = I ; ran = U};;
	val p5 : personne = {nom = "e"; ele = I; ran = U}
	let p8 = {nom = "f"; ele = A ; ran = U};;
	val p8 : personne = {nom = "f"; ele = A; ran = U}
	let p9 = {nom = "g"; ele = A ; ran = U};;
	val p9 : personne = {nom = "g"; ele = A; ran = U}
	let p6 = {nom = "h"; ele = T ; ran = U};;
	val p6 : personne = {nom = "h"; ele = T; ran = U}
	let p7 = {nom = "i"; ele = F ; ran = U};;
	val p7 : personne = {nom = "i"; ele = F; ran = U}
	# 
	let a = Noeud(p4,f,f);;
	val a : arbre = Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille)
	let b = Noeud(p6,f,f);;
	val b : arbre = Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille)
	let c = Noeud(p7,f,f);;
	val c : arbre = Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille)
	let d = Noeud(p8,f,f);;
	val d : arbre = Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille)
	let e = Noeud(p9,f,f);;
	val e : arbre = Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)
	# 
	  
	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = U},
	   Noeud ({nom = "b"; ele = I; ran = U},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = I; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = I; ran = U},
	    Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)))
	let comparaison_PM modif mere pere = match (mere.ele, pere.ele) with
	| (F ,E) -> {nom=modif.nom;ele=N;ran=U}
	| (E ,F) -> {nom=modif.nom;ele=N;ran=U}
	| (A ,T) -> {nom=modif.nom;ele=N;ran=U}
	| (T ,A) -> {nom=modif.nom;ele=N;ran=U}
	| (T ,T) -> {nom=modif.nom;ele=T;ran=U}
	| (A ,A) -> {nom=modif.nom;ele=A;ran=U}
	| (F ,_) -> {nom=modif.nom;ele=F;ran=U}
	| (_ ,F) -> {nom=modif.nom;ele=F;ran=U}
	| (N ,_) -> {nom=modif.nom;ele=E;ran=U}
	| (_ ,N) -> {nom=modif.nom;ele=E;ran=U}
	| (E ,_) -> {nom=modif.nom;ele=E;ran=U}
	| (_ ,E) -> {nom=modif.nom;ele=E;ran=U}
	| (_ ,_) -> {nom=modif.nom;ele=I;ran=U};;
	val comparaison_PM : personne -> personne -> personne -> personne = <fun>
	# 
	let  complet abr = match abr with
	| Feuille -> f
	| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> Noeud(comparaison_PM p1 a d, Noeud(a,b,c), Noeud(d,e,f)) 
	| Noeud (p2,a2,b2) -> Noeud(p2,a2,b2);;
	val complet : arbre -> arbre = <fun>
	# 
	 let rec complet2 abr = match abr with
	| Feuille -> f
	| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) ->if (a.ele = I) then Noeud(p1,complet (complet2 (Noeud(a,b,c))), complet2 (Noeud(d,e,f)) )
	else if (d.ele = I) then Noeud(p1, complet2 (Noeud(a,b,c)), complet (complet2 (Noeud(d,e,f))) )
	else if (p1.ele =I) then complet (Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)))
	else Noeud(p1, complet2 (Noeud(a,b,c)), complet2 (Noeud(d,e,f)))
	 | Noeud (p2,a2,b2) -> Noeud(p2,a2,b2);;
	val complet2 : arbre -> arbre = <fun>
	# 
	  let arbre2 = complet2 arbre;;
	val arbre2 : arbre =
	  Noeud ({nom = "a"; ele = E; ran = U},
	   Noeud ({nom = "b"; ele = N; ran = U},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = F; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = U},
	    Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)))
*)

(*	Partie 5 : Écrivez la (les) fonction(s) nécessaire(s) pour vérifier la crédibilité d’un arbre généalogique.
	Illustrez ce calcul à l’aide d’un appel sur un exemple significatif et mettez en commentaire le
	résultat de l’évaluation OCaml.
*)
(*	Comme pour la précedente partie on a utlisé plusieurs fonction pour réalisé la vérification de l'arbre:
	-possiblite_elem : renvoie une liste l'element en fonction de l'element passé en parametre 
	-contains : vérifie que un element es contenue dans la liste (obtenue par possiblite_elem)
	-verification : vérifie l'élément fils si il correspond a ceux de la combinaison de celle de la mère et du père 
	-empilement_Noeud : empile les noeuds sous forme de triplet pour avoir une plus grande facilité pour manipulé les noeud.
	-verif_arbre: vérification de l'arbre renvoie false si il y a une erreur et true si aucune erreur est presente.
*)

let possiblite_elem elem = match elem with 
| E-> [E;T;A]
| F-> [F;T;A]
| T-> [T]
| A-> [A]
| N-> [E;T;A;F]
| I-> [];;

let rec contains elem l = match l with 
| [] -> false
| a::l -> if a=elem then true else contains elem l;;



let verification fils mere pere = match (fils.ele) with
|E-> contains mere.ele (possiblite_elem mere.ele) || contains pere.ele (possiblite_elem pere.ele)
|F-> contains mere.ele (possiblite_elem mere.ele) || contains pere.ele (possiblite_elem pere.ele)
|A-> contains mere.ele (possiblite_elem mere.ele) && contains pere.ele (possiblite_elem pere.ele)
|T-> contains mere.ele (possiblite_elem mere.ele) && contains pere.ele (possiblite_elem pere.ele)
|N-> mere.ele = F && pere.ele =E || mere.ele = E && pere.ele =F || mere.ele = T && pere.ele = A || mere.ele = A && pere.ele =T  
|I-> false;;

let rec empilement_Noeud abr l = match abr with
| Feuille -> l
| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> (p1,a,d) :: empilement_Noeud (Noeud(a,b,c)) l @ empilement_Noeud (Noeud(d,e,f)) l
| Noeud (p,a,b) -> l;;


let rec verif_arbre = function
[] -> true
| (a,b,c)::l -> if (verification a b c) then true && verif_arbre l else false;;

(*	Exemple d'execution:
	let p1 = {nom = "a"; ele = E ; ran = U};;
	val p1 : personne = {nom = "a"; ele = E; ran = U}
	let p2 = {nom = "b"; ele = I ; ran = U};;
	val p2 : personne = {nom = "b"; ele = I; ran = U}
	let p3 = {nom = "c"; ele = I ; ran = U};;
	val p3 : personne = {nom = "c"; ele = I; ran = U}
	let p4 = {nom = "d"; ele = E ; ran = U};;
	val p4 : personne = {nom = "d"; ele = E; ran = U}
	let p5 = {nom = "e"; ele = I ; ran = U};;
	val p5 : personne = {nom = "e"; ele = I; ran = U}
	let p8 = {nom = "f"; ele = A ; ran = U};;
	val p8 : personne = {nom = "f"; ele = A; ran = U}
	let p9 = {nom = "g"; ele = A ; ran = U};;
	val p9 : personne = {nom = "g"; ele = A; ran = U}
	let p6 = {nom = "h"; ele = T ; ran = U};;
	val p6 : personne = {nom = "h"; ele = T; ran = U}
	let p7 = {nom = "i"; ele = F ; ran = U};;
	val p7 : personne = {nom = "i"; ele = F; ran = U}
	# 
	let a = Noeud(p4,f,f);;
	val a : arbre = Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille)
	let b = Noeud(p6,f,f);;
	val b : arbre = Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille)
	let c = Noeud(p7,f,f);;
	val c : arbre = Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille)
	let d = Noeud(p8,f,f);;
	val d : arbre = Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille)
	let e = Noeud(p9,f,f);;
	val e : arbre = Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)
	# 
	  
	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = U},
	   Noeud ({nom = "b"; ele = I; ran = U},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = I; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = I; ran = U},
	    Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)))
	let comparaison_PM modif mere pere = match (mere.ele, pere.ele) with
	| (F ,E) -> {nom=modif.nom;ele=N;ran=U}
	| (E ,F) -> {nom=modif.nom;ele=N;ran=U}
	| (A ,T) -> {nom=modif.nom;ele=N;ran=U}
	| (T ,A) -> {nom=modif.nom;ele=N;ran=U}
	| (T ,T) -> {nom=modif.nom;ele=T;ran=U}
	| (A ,A) -> {nom=modif.nom;ele=A;ran=U}
	| (F ,_) -> {nom=modif.nom;ele=F;ran=U}
	| (_ ,F) -> {nom=modif.nom;ele=F;ran=U}
	| (N ,_) -> {nom=modif.nom;ele=E;ran=U}
	| (_ ,N) -> {nom=modif.nom;ele=E;ran=U}
	| (E ,_) -> {nom=modif.nom;ele=E;ran=U}
	| (_ ,E) -> {nom=modif.nom;ele=E;ran=U}
	| (_ ,_) -> {nom=modif.nom;ele=I;ran=U};;

	val comparaison_PM : personne -> personne -> personne -> personne = <fun>
	let  complet abr = match abr with
	| Feuille -> f
	| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> Noeud(comparaison_PM p1 a d, Noeud(a,b,c), Noeud(d,e,f)) 
	| Noeud (p2,a2,b2) -> Noeud(p2,a2,b2);;
	val complet : arbre -> arbre = <fun>
	# 
	 let rec complet2 abr = match abr with
	| Feuille -> f
	| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) ->if (a.ele = I) then Noeud(p1,complet (complet2 (Noeud(a,b,c))), complet2 (Noeud(d,e,f)) )
	else if (d.ele = I) then Noeud(p1, complet2 (Noeud(a,b,c)), complet (complet2 (Noeud(d,e,f))) )
	else if (p1.ele =I) then complet (Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)))
	else Noeud(p1, complet2 (Noeud(a,b,c)), complet2 (Noeud(d,e,f)))
	 | Noeud (p2,a2,b2) -> Noeud(p2,a2,b2);;
	val complet2 : arbre -> arbre = <fun>
	# 
	let arbre2 = complet2 arbre;;

	val arbre2 : arbre =
	  Noeud ({nom = "a"; ele = E; ran = U},
	   Noeud ({nom = "b"; ele = N; ran = U},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = F; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = U},
	    Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)))
	let possiblite_elem elem = match elem with 
	| E-> [E;T;A]
	| F-> [F;T;A]
	| T-> [T]
	| A-> [A]
	| N-> [E;T;A;F]
	| I-> [];;
	val possiblite_elem : element -> element list = <fun>
	# 
	let rec contains elem l = match l with 
	| [] -> false
	| a::l -> if a=elem then true else contains elem l;;
	val contains : 'a -> 'a list -> bool = <fun>
	# 
	  
	  
	let verification fils mere pere = match (fils.ele) with
	|E-> contains mere.ele (possiblite_elem mere.ele) || contains pere.ele (possiblite_elem pere.ele)
	|F-> contains mere.ele (possiblite_elem mere.ele) || contains pere.ele (possiblite_elem pere.ele)
	|A-> contains mere.ele (possiblite_elem mere.ele) && contains pere.ele (possiblite_elem pere.ele)
	|T-> contains mere.ele (possiblite_elem mere.ele) && contains pere.ele (possiblite_elem pere.ele)
	|N-> mere.ele = F && pere.ele =E || mere.ele = E && pere.ele =F || mere.ele = T && pere.ele = A || mere.ele = A && pere.ele =T  
	|I-> false;;
	val verification : personne -> personne -> personne -> bool = <fun>
	# 
	let rec empilement_Noeud abr l = match abr with
	| Feuille -> l
	| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> (p1,a,d) :: empilement_Noeud (Noeud(a,b,c)) l @ empilement_Noeud (Noeud(d,e,f)) l
	| Noeud (p,a,b) -> l;;
	val empilement_Noeud :
	  arbre ->
	  (personne * personne * personne) list ->
	  (personne * personne * personne) list = <fun>
	# 
	  
	let rec verif_arbre = function
	[] -> true
	| (a,b,c)::l -> if (verification a b c) then true && verif_arbre l else false;;
	val verif_arbre : (personne * personne * personne) list -> bool = <fun>
	# 
	let p1 = {nom = "a"; ele = E ; ran = U};;
	val p1 : personne = {nom = "a"; ele = E; ran = U}
	let p2 = {nom = "b"; ele = N ; ran = U};;
	val p2 : personne = {nom = "b"; ele = N; ran = U}
	let p3 = {nom = "c"; ele = A ; ran = U};;
	val p3 : personne = {nom = "c"; ele = A; ran = U}
	let p4 = {nom = "d"; ele = E ; ran = U};;
	val p4 : personne = {nom = "d"; ele = E; ran = U}
	let p5 = {nom = "e"; ele = E ; ran = U};;
	val p5 : personne = {nom = "e"; ele = E; ran = U}
	let p8 = {nom = "f"; ele = A ; ran = U};;
	val p8 : personne = {nom = "f"; ele = A; ran = U}
	let p9 = {nom = "g"; ele = A ; ran = U};;
	val p9 : personne = {nom = "g"; ele = A; ran = U}
	let p6 = {nom = "h"; ele = T ; ran = U};;
	val p6 : personne = {nom = "h"; ele = T; ran = U}
	let p7 = {nom = "i"; ele = F ; ran = U};;
	val p7 : personne = {nom = "i"; ele = F; ran = U}
	# 
	let a = Noeud(p4,f,f);;
	val a : arbre = Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille)
	let b = Noeud(p6,f,f);;
	val b : arbre = Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille)
	let c = Noeud(p7,f,f);;
	val c : arbre = Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille)
	let d = Noeud(p8,f,f);;
	val d : arbre = Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille)
	let e = Noeud(p9,f,f);;
	val e : arbre = Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)
	# 
	  
	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;
	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = U},
	   Noeud ({nom = "b"; ele = N; ran = U},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = E; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = U},
	    Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)))
	# let list2 = empilement_Noeud arbre [];;
	val list2 : (personne * personne * personne) list =
	  [({nom = "a"; ele = E; ran = U}, {nom = "b"; ele = N; ran = U},
	    {nom = "c"; ele = A; ran = U});
	   ({nom = "b"; ele = N; ran = U}, {nom = "d"; ele = E; ran = U},
	    {nom = "e"; ele = E; ran = U});
	   ({nom = "e"; ele = E; ran = U}, {nom = "h"; ele = T; ran = U},
	    {nom = "i"; ele = F; ran = U});
	   ({nom = "c"; ele = A; ran = U}, {nom = "f"; ele = A; ran = U},
	    {nom = "g"; ele = A; ran = U})]
	verif_arbre list2;;
	- : bool = false
	# 
	let list = empilement_Noeud arbre2 [];;
	val list : (personne * personne * personne) list =
	  [({nom = "a"; ele = E; ran = U}, {nom = "b"; ele = N; ran = U},
	    {nom = "c"; ele = A; ran = U});
	   ({nom = "b"; ele = N; ran = U}, {nom = "d"; ele = E; ran = U},
	    {nom = "e"; ele = F; ran = U});
	   ({nom = "e"; ele = F; ran = U}, {nom = "h"; ele = T; ran = U},
	    {nom = "i"; ele = F; ran = U});
	   ({nom = "c"; ele = A; ran = U}, {nom = "f"; ele = A; ran = U},
	    {nom = "g"; ele = A; ran = U})]
	# verif_arbre list;;
	- : bool = true
*)

(*Partie 6 : Faites une 2ème version de la (les) fonction(s) nécessaire(s) pour afficher un arbre généa-
	logique en prenant en compte les rangs. Veillez à ne faire qu’un unique appel non fonctionnel.
	Illustrez l’affichage à l’aide d’un appel sur un exemple significatif et mettez en commentaire
	le résultat de l’évaluation OCaml.
	Faites une 2ème version de la (les) fonction(s) de vérification en prenant également en
	compte les rangs. Illustrez ce calcul à l’aide d’un appel sur un exemple significatif et mettez
	en commentaire le résultat de l’évaluation OCaml.
*)

(*	Concernant l'affichage on s'est basé sur le meme principe que celui du non rang 
	on a juste rendue la fonction récursive pour effectué un affichage d'une personne on 
	connait l'élément mais pas le rang. Concerant la vérification du rang on se base sur le
	même pricipe sur que sur la vérification des éléments. (on utilisara l'empilement des noeuds
	de la partie précédente pour cette partie aussi).
*)

let verification_rang fils mere pere = match (fils.ran,mere.ran,pere.ran) with
| (_, U, _) |(_,_,U) | (U,_,_)  -> true 
| (P,_,_) -> true
| (M,M,M) -> true
| (_,_,_) -> false;;

let rec verif_arbre_rang = function
[] -> true
| (a,b,c)::l -> if (verification_rang a b c) then true && verif_arbre_rang l else false;;

(*	Exemple d'execution :
	let verification_rang fils mere pere = match (fils.ran,mere.ran,pere.ran) with
	| (_, U, _) |(_,_,U)  -> false 
	| (P,_,_) -> true
	| (M,M,M) -> true
	| (_,_,_) -> false;;

	val verification_rang : personne -> personne -> personne -> bool = <fun>
	let rec verif_arbre_rang = function
	[] -> true
	| (a,b,c)::l -> if (verification_rang a b c) then true && verif_arbre_rang l else false;;
	val verif_arbre_rang : (personne * personne * personne) list -> bool = <fun>
	# 
	let p1 = {nom = "a"; ele = E ; ran = P};;
	val p1 : personne = {nom = "a"; ele = E; ran = P}
	let p2 = {nom = "b"; ele = N ; ran = P};;
	val p2 : personne = {nom = "b"; ele = N; ran = P}
	let p3 = {nom = "c"; ele = A ; ran = P};;
	val p3 : personne = {nom = "c"; ele = A; ran = P}
	let p4 = {nom = "d"; ele = E ; ran = M};;
	val p4 : personne = {nom = "d"; ele = E; ran = M}
	let p5 = {nom = "e"; ele = E ; ran = M};;
	val p5 : personne = {nom = "e"; ele = E; ran = M}
	let p8 = {nom = "f"; ele = A ; ran = M};;
	val p8 : personne = {nom = "f"; ele = A; ran = M}
	let p9 = {nom = "g"; ele = A ; ran = P};;
	val p9 : personne = {nom = "g"; ele = A; ran = P}
	let p6 = {nom = "h"; ele = T ; ran = P};;
	val p6 : personne = {nom = "h"; ele = T; ran = P}
	let p7 = {nom = "i"; ele = F ; ran = P};;
	val p7 : personne = {nom = "i"; ele = F; ran = P}
	# 
	let a = Noeud(p4,f,f);;
	val a : arbre = Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille)
	let b = Noeud(p6,f,f);;
	val b : arbre = Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille)
	# let c = Noeud(p7,f,f);;
	val c : arbre = Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille)
	let d = Noeud(p8,f,f);;
	val d : arbre = Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille)
	# let e = Noeud(p9,f,f);;
	val e : arbre = Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)
	# 
	  
	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = P},
	   Noeud ({nom = "b"; ele = N; ran = P},
	    Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = E; ran = M},
	     Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = P},
	    Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)))
	let list1 = empilement_Noeud arbre [];;
	verif_arbre_rang list1;;
	val list1 : (personne * personne * personne) list =
	  [({nom = "a"; ele = E; ran = P}, {nom = "b"; ele = N; ran = P},
	    {nom = "c"; ele = A; ran = P});
	   ({nom = "b"; ele = N; ran = P}, {nom = "d"; ele = E; ran = M},
	    {nom = "e"; ele = E; ran = M});
	   ({nom = "e"; ele = E; ran = M}, {nom = "h"; ele = T; ran = P},
	    {nom = "i"; ele = F; ran = P});
	   ({nom = "c"; ele = A; ran = P}, {nom = "f"; ele = A; ran = M},
	    {nom = "g"; ele = A; ran = P})]
	# - : bool = false
	# 
	let p1 = {nom = "a"; ele = E ; ran = P};;
	val p1 : personne = {nom = "a"; ele = E; ran = P}
	let p2 = {nom = "b"; ele = N ; ran = P};;
	val p2 : personne = {nom = "b"; ele = N; ran = P}
	let p3 = {nom = "c"; ele = A ; ran = P};;
	val p3 : personne = {nom = "c"; ele = A; ran = P}
	let p4 = {nom = "d"; ele = E ; ran = M};;
	val p4 : personne = {nom = "d"; ele = E; ran = M}
	let p5 = {nom = "e"; ele = E ; ran = P};;
	val p5 : personne = {nom = "e"; ele = E; ran = P}
	let p8 = {nom = "f"; ele = A ; ran = M};;
	val p8 : personne = {nom = "f"; ele = A; ran = M}
	let p9 = {nom = "g"; ele = A ; ran = P};;
	val p9 : personne = {nom = "g"; ele = A; ran = P}
	let p6 = {nom = "h"; ele = T ; ran = P};;
	val p6 : personne = {nom = "h"; ele = T; ran = P}
	let p7 = {nom = "i"; ele = F ; ran = P};;
	val p7 : personne = {nom = "i"; ele = F; ran = P}
	# 
	let a = Noeud(p4,f,f);;
	val a : arbre = Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille)
	let b = Noeud(p6,f,f);;
	val b : arbre = Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille)
	let c = Noeud(p7,f,f);;
	val c : arbre = Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille)
	let d = Noeud(p8,f,f);;
	val d : arbre = Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille)
	let e = Noeud(p9,f,f);;
	val e : arbre = Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)
	# 
	  
	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;
	let list2 = empilement_Noeud arbre [];;
	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = P},
	   Noeud ({nom = "b"; ele = N; ran = P},
	    Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = E; ran = P},
	     Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = P},
	    Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)))
	# val list2 : (personne * personne * personne) list =
	  [({nom = "a"; ele = E; ran = P}, {nom = "b"; ele = N; ran = P},
	    {nom = "c"; ele = A; ran = P});
	   ({nom = "b"; ele = N; ran = P}, {nom = "d"; ele = E; ran = M},
	    {nom = "e"; ele = E; ran = P});
	   ({nom = "e"; ele = E; ran = P}, {nom = "h"; ele = T; ran = P},
	    {nom = "i"; ele = F; ran = P});
	   ({nom = "c"; ele = A; ran = P}, {nom = "f"; ele = A; ran = M},
	    {nom = "g"; ele = A; ran = P})]
	# verif_arbre_rang list2;;
	- : bool = true
*)


(*Partie 7 :Ecrivez les fonctions nécessaires pour trouver le nom du plus vieil individu d’un élément
	donné dans un arbre. Illustrez ce calcul à l’aide d’un appel sur un exemple significatif et mettez
	en commentaire le résultat de l’évaluation OCaml.
*)

(*	Pour pouvoir retrouvé l'élément le plus ancien on opte pour la stratégie suivante :
	-on effectue un parcours en largeur de l'arbre grace au fonction suivante:
		.hauteur qui calcule la hauteur de l'arbre
		.par_niveau qui renvoie une liste de tout les neouds d'un niveau de l'arbre
		.parcours_en_largeur qui reunis les deux fonctions pour effectuer le parcours et renvoie une liste de liste de noeud
	-On concatenne la liste des niveau pour une liste principale des ancètres par la définition let lili = List.flatten li;;
	-On renverse cette liste pour que les premieres personnes soit dans l'ordre des plus anciens
	-Puis on parcours la liste a la recherche de l'element passé en parametre

*)

let rec hauteur = function
| Feuille -> 0
| Noeud(_,g,d) -> 1 + (max (hauteur g) (hauteur d)) ;;

let rec par_niveau t l = match t with
| Feuille -> []
| Noeud(c, a, b) ->
    if l = 1 then [c]
    else par_niveau a (l - 1) @ par_niveau b (l - 1);;

let rec parcours_en_largeur arbre haut l =
	if haut = 0 then l
	else parcours_en_largeur arbre (haut-1) ((par_niveau arbre haut) :: l) ;;

let li =parcours_en_largeur arbre hau [];;

let lili = List.flatten li;;

let rec renverse l1 l2 = match l2 with
[] -> l1
| a :: tl -> renverse (a :: l1) tl;;

let rec recherche_ancetre n= function
[] -> ""
|p::l -> if n=p.ele then p.nom else recherche_ancetre n l;;

(*	Exemple d'application :
	let rec hauteur = function
	| Feuille -> 0
	| Noeud(_,g,d) -> 1 + (max (hauteur g) (hauteur d)) ;;

	let rec par_niveau t l = match t with
	| Feuille -> []
	| Noeud(c, a, b) ->
	    if l = 1 then [c]
	    else par_niveau a (l - 1) @ par_niveau b (l - 1);;

	let rec parcours_en_largeur arbre haut l =
		if haut = 0 then l
		else parcours_en_largeur arbre (haut-1) ((par_niveau arbre haut) :: l) ;;

	let li =parcours_en_largeur arbre hau [];;

	let lili = List.flatten li;;

	let rec renverse l1 l2 = match l2 with
	[] -> l1
	| a :: tl -> renverse (a :: l1) tl;;

	let rec recherche_ancetre n= function
	[] -> ""
	|p::l -> if n=p.ele then p.nom else recherche_ancetre n l;;

	let p1 = {nom = "a"; ele = E ; ran = P};;
	let p2 = {nom = "b"; ele = N ; ran = P};;
	let p3 = {nom = "c"; ele = A ; ran = P};;
	let p4 = {nom = "d"; ele = E ; ran = M};;
	let p5 = {nom = "e"; ele = E ; ran = P};;
	let p8 = {nom = "f"; ele = A ; ran = M};;
	let p9 = {nom = "g"; ele = A ; ran = P};;
	let p6 = {nom = "h"; ele = T ; ran = P};;
	let p7 = {nom = "i"; ele = F ; ran = P};;

	let a = Noeud(p4,f,f);;
	let b = Noeud(p6,f,f);;
	let c = Noeud(p7,f,f);;
	let d = Noeud(p8,f,f);;
	let e = Noeud(p9,f,f);;


	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

	let hau = hauteur arbre;;
	let li =parcours_en_largeur arbre hau [];;

	let lili = List.flatten li;;

	let list_ancetre = renverse [] lili;;
	recherche_ancetre E list_ancetre;;
	recherche_ancetre I list_ancetre;;
	recherche_ancetre F list_ancetre;;
	recherche_ancetre N list_ancetre;;
*)

(*Partie 8 :(a) Implémentez ces fonctions de modification et la fonction qui les applique. Illustrez
	l’application de chacun des modifications à l’aide d’un appel sur un exemple significatif et
	mettez en commentaire le résultat de l’évaluation OCaml (il y a 4 appels).
	
	(b) Implémentez des compositions de ces fonctions, qui ne parcourent le graphe qu’une seule
	fois. Par exemple, changer tous les air en feu ET inverser le rang, ou encore faire le miroir ET
	supprimer tous les ascendants d’un individu sans pouvoir. Illustrez ces deux compositions à
	l’aide d’appels sur un exemple significatif et mettez en commentaire le résultat de l’évaluation
	OCaml.
*)

(* Concernant les fonction d'ordre suppérieur on va faire un bref résumé:
	- miroir : on a en paramètre un noeud Noeud(e,x,y) on renvoie un noeud Noeud(e,y,x)
	- supp:  on a en paramètre un noeud Noeud(e,x,y) si e.ele = N alors on renvoie un Noeud(e,Feuille, Feuille) sinon on renvoit
	le noeud sans modification
	-modif_elem = on a en paramètre un noeud Noeud(e,x,y)si e.ele = A on renvoie un noeud Noeud({nom = e.nom ; ele = F ; ran = e.ran}, x, y)
	sinon on renvoie le noeud sans modification.
	-modif_rang : on a en paramètre un noeud Noeud(e,x,y)si e.ran = P on renvoie un noeud Noeud({nom = e.nom ; ele = e.ele; ran = M}, x, y)
	sinon on renvoie Noeud({nom = e.nom ; ele = e.ele; ran = P}, x, y)
	
	Concerant la composition de fonction on passe en paramètre 2 fonctions et l'arbre on effectue la premiere fonction sur le fils du
	noeud puis la seconde fonction et on fais un appele recursif sur les branche de l'arbre.
*)

let miroir a = match a with
Feuille -> Feuille
| Noeud(e,x,y)-> Noeud(e, y , x);;


let supp a = match a with 
Feuille -> Feuille
| Noeud (e,x,y) -> if e.ele = N then Noeud(e,f,f) else Noeud(e, x, y);;


let modif_elem a = match  a with
| Feuille -> Feuille
| Noeud(e,x,y) -> if e.ele = A then Noeud({nom = e.nom ; ele = F ; ran = e.ran}, x, y) else  Noeud(e, x, y);;

let modif_rang a = match a with 
| Feuille -> Feuille
| Noeud(e,x,y) -> if e.ran = P then Noeud({nom = e.nom ; ele = e.ele; ran = M}, x, y) else  Noeud({nom = e.nom ; ele = e.ele; ran = P}, x, y);;

let rec ordre_sup f1 f2 a = match a with
  | Feuille -> Feuille
  | Noeud(e,x,y) -> f2 (f1 (Noeud(e, ordre_sup f2 f1 x , ordre_sup f2 f1 y)));;

(* Exemple de d'execution :
	let miroir a = match a with
	Feuille -> Feuille
	| Noeud(e,x,y)-> Noeud(e, y , x);;
	val miroir : arbre -> arbre = <fun>
	# 
	  
	  let supp a = match a with 
	  Feuille -> Feuille
	  | Noeud (e,x,y) -> if e.ele = N then Noeud(e,f,f) else Noeud(e, x, y);;
	Error: This expression has type arbre -> arbre
	       but an expression was expected of type arbre
	# 
	  
	let modif_elem a = match  a with
	| Feuille -> Feuille
	| Noeud(e,x,y) -> if e.ele = A then Noeud({nom = e.nom ; ele = F ; ran = e.ran}, x, y) else  Noeud(e, x, y);;
	val modif_elem : arbre -> arbre = <fun>
	# 
	let modif_rang a = match a with 
	| Feuille -> Feuille
	| Noeud(e,x,y) -> if e.ran = P then Noeud({nom = e.nom ; ele = e.ele; ran = M}, x, y) else  Noeud({nom = e.nom ; ele = e.ele; ran = P}, x, y);;
	val modif_rang : arbre -> arbre = <fun>
	# 
	let rec ordre_sup f1 f2 a = match a with
	  | Feuille -> Feuille
	  | Noeud(e,x,y) -> f2 (f1 (Noeud(e, ordre_sup f2 f1 x , ordre_sup f2 f1 y)));;
	val ordre_sup : (arbre -> arbre) -> (arbre -> arbre) -> arbre -> arbre =
	  <fun>
	# 
	let p1 = {nom = "a"; ele = E ; ran = P};;
	val p1 : personne = {nom = "a"; ele = E; ran = P}
	let p2 = {nom = "b"; ele = N ; ran = P};;
	val p2 : personne = {nom = "b"; ele = N; ran = P}
	let p3 = {nom = "c"; ele = A ; ran = P};;
	val p3 : personne = {nom = "c"; ele = A; ran = P}
	let p4 = {nom = "d"; ele = E ; ran = M};;
	val p4 : personne = {nom = "d"; ele = E; ran = M}
	let p5 = {nom = "e"; ele = E ; ran = P};;
	val p5 : personne = {nom = "e"; ele = E; ran = P}
	let p8 = {nom = "f"; ele = A ; ran = M};;
	val p8 : personne = {nom = "f"; ele = A; ran = M}
	let p9 = {nom = "g"; ele = A ; ran = P};;
	val p9 : personne = {nom = "g"; ele = A; ran = P}
	let p6 = {nom = "h"; ele = T ; ran = P};;
	val p6 : personne = {nom = "h"; ele = T; ran = P}
	let p7 = {nom = "i"; ele = F ; ran = P};;
	val p7 : personne = {nom = "i"; ele = F; ran = P}
	# 
	  let a = Noeud(p4,f,f);;
	Error: This expression has type arbre -> arbre
	       but an expression was expected of type arbre
	# let b = Noeud(p6,f,f);;
	Error: This expression has type arbre -> arbre
	       but an expression was expected of type arbre
	# let c = Noeud(p7,f,f);;
	Error: This expression has type arbre -> arbre
	       but an expression was expected of type arbre
	# let d = Noeud(p8,f,f);;
	Error: This expression has type arbre -> arbre
	       but an expression was expected of type arbre
	# let e = Noeud(p9,f,f);;
	Error: This expression has type arbre -> arbre
	       but an expression was expected of type arbre
	# 
	  
	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = P},
	   Noeud ({nom = "b"; ele = N; ran = P},
	    Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = E; ran = P},
	     Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = P},
	    Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)))
	ordre_sup miroir supp arbre;;
	- : arbre =
	Noeud ({nom = "a"; ele = E; ran = P},
	 Noeud ({nom = "c"; ele = A; ran = P},
	  Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille),
	  Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille)),
	 Noeud ({nom = "b"; ele = N; ran = P}, Feuille, Feuille))
	# 
	  ordre_sup modif_rang modif_elem arbre;;
	- : arbre =
	Noeud ({nom = "a"; ele = E; ran = M},
	 Noeud ({nom = "b"; ele = N; ran = M},
	  Noeud ({nom = "d"; ele = E; ran = P}, Feuille, Feuille),
	  Noeud ({nom = "e"; ele = E; ran = M},
	   Noeud ({nom = "h"; ele = T; ran = M}, Feuille, Feuille),
	   Noeud ({nom = "i"; ele = F; ran = M}, Feuille, Feuille))),
	 Noeud ({nom = "c"; ele = F; ran = M},
	  Noeud ({nom = "f"; ele = F; ran = P}, Feuille, Feuille),
	  Noeud ({nom = "g"; ele = F; ran = M}, Feuille, Feuille)))
*)


(*Execution complete des fonctions :

	type element = E | F | A | T | N | I;;
type element = E | F | A | T | N | I
type rang = P | M | U;;
type rang = P | M | U
type personne = {nom : string ; ele : element; ran : rang};;
type personne = { nom : string; ele : element; ran : rang; }
  
  h
type arbre =
    | Feuille
    | Noeud of personne * arbre * arbre;;
type arbre = Feuille | Noeud of personne * arbre * arbre
let aff_ele = function                                        
| E -> "eau"
| F -> "feu"
| A -> "air"
| T -> "terre"
| N -> "aucun"
| I -> "?";;
	val aff_ele : element -> string = <fun>
	let aff_perso_no_rang p =
	  p.nom ^ "[" ^ aff_ele p.ele ^ "]";;
	val aff_perso_no_rang : personne -> string = <fun>
	let rec aff_arbre_no_rang = function
	| Feuille -> ""
	| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> aff_perso_no_rang p1 ^ "(" ^ aff_arbre_no_rang (Noeud(a,b,c)) ^
	  " , " ^ aff_arbre_no_rang (Noeud(d,e,f)) ^ ")"
	| Noeud (p1,Noeud(g,h,i),Feuille) -> aff_perso_no_rang p1 ^ "(" ^ aff_arbre_no_rang (Noeud(g,h,i)) ^
	  " , "
	| Noeud (p1,Feuille,Noeud(k,l,m)) -> aff_perso_no_rang p1 ^ aff_arbre_no_rang (Noeud(k,l,m)) ^
	  ")"
	| Noeud (p1,Feuille,Feuille) -> aff_perso_no_rang p1;;

	val aff_arbre_no_rang : arbre -> string = <fun>
	#   
	let aff_arbre_no_rang_complet abr =
	  print_string (aff_arbre_no_rang abr);;
	val aff_arbre_no_rang_complet : arbre -> unit = <fun>
	# 
	  
	let p1 = {nom = "a"; ele = E ; ran = U};;
	val p1 : personne = {nom = "a"; ele = E; ran = U}
	let p2 = {nom = "b"; ele = N ; ran = U};;
	val p2 : personne = {nom = "b"; ele = N; ran = U}
	let p3 = {nom = "c"; ele = A ; ran = U};;
	val p3 : personne = {nom = "c"; ele = A; ran = U}
	let p4 = {nom = "d"; ele = E ; ran = U};;
	val p4 : personne = {nom = "d"; ele = E; ran = U}
	let p5 = {nom = "e"; ele = F ; ran = U};;
	val p5 : personne = {nom = "e"; ele = F; ran = U}
	let p6 = {nom = "h"; ele = T ; ran = U};;
	val p6 : personne = {nom = "h"; ele = T; ran = U}
	let p7 = {nom = "i"; ele = I ; ran = U};;
	val p7 : personne = {nom = "i"; ele = I; ran = U}
	# 
	  
	  
	let f = Feuille;;
	val f : arbre = Feuille
	let a = Noeud(p6,f,f);;
	val a : arbre = Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille)
	let b = Noeud(p7,f,f);;
	val b : arbre = Noeud ({nom = "i"; ele = I; ran = U}, Feuille, Feuille)
	let c = Noeud(p3,f,f);;
	val c : arbre = Noeud ({nom = "c"; ele = A; ran = U}, Feuille, Feuille)
	let d = Noeud(p4,f,f);;
	val d : arbre = Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille)
	# 
	let arbre = Noeud(p1,Noeud(p2, d,Noeud(p5,a, b)), c);;
	aff_arbre_no_rang_complet arbre;;
	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = U},
	   Noeud ({nom = "b"; ele = N; ran = U},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = F; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = I; ran = U}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = U}, Feuille, Feuille))
	# a[eau](b[aucun](d[eau] , e[feu](h[terre] , i[?])) , c[air])- : unit = ()
	# 
	let rec compteur_elem el abr = match abr with
	| Feuille -> 0
	| Noeud (p1,a1,a2) -> if p1.ele = el then 1 + (compteur_elem el a1) 
	  + (compteur_elem el a2)
	  else (compteur_elem el a1) + (compteur_elem el a2);; 
	val compteur_elem : element -> arbre -> int = <fun>
	# 
	compteur_elem E arbre;;
	- : int = 2
	# 
	compteur_elem F arbre;;
	- : int = 1
	# 
	let rec empilement abr l = match abr with
	| Feuille -> l
	| Noeud (p1,a1,a2) -> p1 :: empilement a1 l @ empilement a2 l;;
	val empilement : arbre -> personne list -> personne list = <fun>
	# 
	let rec recherche_elem_perso n= function
	[] -> I
	|p::l -> if n=p.nom then p.ele else recherche_elem_perso n l;;
	val recherche_elem_perso : string -> personne list -> element = <fun>
	# 
	recherche_elem_perso "e" (empilement arbre []);;
	- : element = F
	recherche_elem_perso "z" (empilement arbre []);;
	- : element = I
	# 
	  
	let p1 = {nom = "a"; ele = E ; ran = U};;
	val p1 : personne = {nom = "a"; ele = E; ran = U}
	let p2 = {nom = "b"; ele = I ; ran = U};;
	val p2 : personne = {nom = "b"; ele = I; ran = U}
	let p3 = {nom = "c"; ele = I ; ran = U};;
	val p3 : personne = {nom = "c"; ele = I; ran = U}
	let p4 = {nom = "d"; ele = E ; ran = U};;
	val p4 : personne = {nom = "d"; ele = E; ran = U}
	let p5 = {nom = "e"; ele = I ; ran = U};;
	val p5 : personne = {nom = "e"; ele = I; ran = U}
	let p8 = {nom = "f"; ele = A ; ran = U};;
	val p8 : personne = {nom = "f"; ele = A; ran = U}
	let p9 = {nom = "g"; ele = A ; ran = U};;
	val p9 : personne = {nom = "g"; ele = A; ran = U}
	let p6 = {nom = "h"; ele = T ; ran = U};;
	val p6 : personne = {nom = "h"; ele = T; ran = U}
	let p7 = {nom = "i"; ele = F ; ran = U};;
	val p7 : personne = {nom = "i"; ele = F; ran = U}
	# 
	let a = Noeud(p4,f,f);;
	val a : arbre = Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille)
	let b = Noeud(p6,f,f);;
	val b : arbre = Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille)
	let c = Noeud(p7,f,f);;
	val c : arbre = Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille)
	let d = Noeud(p8,f,f);;
	val d : arbre = Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille)
	let e = Noeud(p9,f,f);;
	val e : arbre = Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)
	# 
	  
	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = U},
	   Noeud ({nom = "b"; ele = I; ran = U},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = I; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = I; ran = U},
	    Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)))
	let comparaison_PM modif mere pere = match (mere.ele, pere.ele) with
	| (F ,E) -> {nom=modif.nom;ele=N;ran=U}
	| (E ,F) -> {nom=modif.nom;ele=N;ran=U}
	| (A ,T) -> {nom=modif.nom;ele=N;ran=U}
	| (T ,A) -> {nom=modif.nom;ele=N;ran=U}
	| (T ,T) -> {nom=modif.nom;ele=T;ran=U}
	| (A ,A) -> {nom=modif.nom;ele=A;ran=U}
	| (F ,_) -> {nom=modif.nom;ele=F;ran=U}
	| (_ ,F) -> {nom=modif.nom;ele=F;ran=U}
	| (N ,_) -> {nom=modif.nom;ele=E;ran=U}
	| (_ ,N) -> {nom=modif.nom;ele=E;ran=U}
	| (E ,_) -> {nom=modif.nom;ele=E;ran=U}
	| (_ ,E) -> {nom=modif.nom;ele=E;ran=U}
	| (_ ,_) -> {nom=modif.nom;ele=I;ran=U};;

	val comparaison_PM : personne -> personne -> personne -> personne = <fun>
	let  complet abr = match abr with
	| Feuille -> f
	| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> Noeud(comparaison_PM p1 a d, Noeud(a,b,c), Noeud(d,e,f)) 
	| Noeud (p2,a2,b2) -> Noeud(p2,a2,b2);;
	val complet : arbre -> arbre = <fun>
	# 
	 let rec complet2 abr = match abr with
	| Feuille -> f
	| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) ->if (a.ele = I) then Noeud(p1,complet (complet2 (Noeud(a,b,c))), complet2 (Noeud(d,e,f)) )
	else if (d.ele = I) then Noeud(p1, complet2 (Noeud(a,b,c)), complet (complet2 (Noeud(d,e,f))) )
	else if (p1.ele =I) then complet (Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)))
	else Noeud(p1, complet2 (Noeud(a,b,c)), complet2 (Noeud(d,e,f)))
	 | Noeud (p2,a2,b2) -> Noeud(p2,a2,b2);;
	val complet2 : arbre -> arbre = <fun>
	# 
	let arbre2 = complet2 arbre;;

	val arbre2 : arbre =
	  Noeud ({nom = "a"; ele = E; ran = U},
	   Noeud ({nom = "b"; ele = N; ran = U},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = F; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = U},
	    Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)))
	let possiblite_elem elem = match elem with 
	| E-> [E;T;A]
	| F-> [F;T;A]
	| T-> [T]
	| A-> [A]
	| N-> [E;T;A;F]
	| I-> [];;
	val possiblite_elem : element -> element list = <fun>
	# 
	let rec contains elem l = match l with 
	| [] -> false
	| a::l -> if a=elem then true else contains elem l;;
	val contains : 'a -> 'a list -> bool = <fun>
	# 
	  
	  
	let verification fils mere pere = match (fils.ele) with
	|E-> contains mere.ele (possiblite_elem mere.ele) || contains pere.ele (possiblite_elem pere.ele)
	|F-> contains mere.ele (possiblite_elem mere.ele) || contains pere.ele (possiblite_elem pere.ele)
	|A-> contains mere.ele (possiblite_elem mere.ele) && contains pere.ele (possiblite_elem pere.ele)
	|T-> contains mere.ele (possiblite_elem mere.ele) && contains pere.ele (possiblite_elem pere.ele)
	|N-> mere.ele = F && pere.ele =E || mere.ele = E && pere.ele =F || mere.ele = T && pere.ele = A || mere.ele = A && pere.ele =T  
	|I-> false;;
	val verification : personne -> personne -> personne -> bool = <fun>
	# 
	let rec empilement_Noeud abr l = match abr with
	| Feuille -> l
	| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> (p1,a,d) :: empilement_Noeud (Noeud(a,b,c)) l @ empilement_Noeud (Noeud(d,e,f)) l
	| Noeud (p,a,b) -> l;;
	val empilement_Noeud :
	  arbre ->
	  (personne * personne * personne) list ->
	  (personne * personne * personne) list = <fun>
	# 
	  
	let rec verif_arbre = function
	[] -> true
	| (a,b,c)::l -> if (verification a b c) then true && verif_arbre l else false;;
	val verif_arbre : (personne * personne * personne) list -> bool = <fun>
	# 
	let p1 = {nom = "a"; ele = E ; ran = U};;
	val p1 : personne = {nom = "a"; ele = E; ran = U}
	let p2 = {nom = "b"; ele = N ; ran = U};;
	val p2 : personne = {nom = "b"; ele = N; ran = U}
	let p3 = {nom = "c"; ele = A ; ran = U};;
	val p3 : personne = {nom = "c"; ele = A; ran = U}
	let p4 = {nom = "d"; ele = E ; ran = U};;
	val p4 : personne = {nom = "d"; ele = E; ran = U}
	let p5 = {nom = "e"; ele = E ; ran = U};;
	val p5 : personne = {nom = "e"; ele = E; ran = U}
	let p8 = {nom = "f"; ele = A ; ran = U};;
	val p8 : personne = {nom = "f"; ele = A; ran = U}
	let p9 = {nom = "g"; ele = A ; ran = U};;
	val p9 : personne = {nom = "g"; ele = A; ran = U}
	let p6 = {nom = "h"; ele = T ; ran = U};;
	val p6 : personne = {nom = "h"; ele = T; ran = U}
	let p7 = {nom = "i"; ele = F ; ran = U};;
	val p7 : personne = {nom = "i"; ele = F; ran = U}
	# 
	let a = Noeud(p4,f,f);;
	val a : arbre = Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille)
	let b = Noeud(p6,f,f);;
	val b : arbre = Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille)
	let c = Noeud(p7,f,f);;
	val c : arbre = Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille)
	let d = Noeud(p8,f,f);;
	val d : arbre = Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille)
	let e = Noeud(p9,f,f);;
	val e : arbre = Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)
	# 
	  
	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;
	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = U},
	   Noeud ({nom = "b"; ele = N; ran = U},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = E; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = U}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = U}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = U},
	    Noeud ({nom = "f"; ele = A; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = U}, Feuille, Feuille)))
	# let list2 = empilement_Noeud arbre [];;
	val list2 : (personne * personne * personne) list =
	  [({nom = "a"; ele = E; ran = U}, {nom = "b"; ele = N; ran = U},
	    {nom = "c"; ele = A; ran = U});
	   ({nom = "b"; ele = N; ran = U}, {nom = "d"; ele = E; ran = U},
	    {nom = "e"; ele = E; ran = U});
	   ({nom = "e"; ele = E; ran = U}, {nom = "h"; ele = T; ran = U},
	    {nom = "i"; ele = F; ran = U});
	   ({nom = "c"; ele = A; ran = U}, {nom = "f"; ele = A; ran = U},
	    {nom = "g"; ele = A; ran = U})]
	verif_arbre list2;;
	- : bool = false
	# 
	let list = empilement_Noeud arbre2 [];;
	val list : (personne * personne * personne) list =
	  [({nom = "a"; ele = E; ran = U}, {nom = "b"; ele = N; ran = U},
	    {nom = "c"; ele = A; ran = U});
	   ({nom = "b"; ele = N; ran = U}, {nom = "d"; ele = E; ran = U},
	    {nom = "e"; ele = F; ran = U});
	   ({nom = "e"; ele = F; ran = U}, {nom = "h"; ele = T; ran = U},
	    {nom = "i"; ele = F; ran = U});
	   ({nom = "c"; ele = A; ran = U}, {nom = "f"; ele = A; ran = U},
	    {nom = "g"; ele = A; ran = U})]
	# verif_arbre list;;
	- : bool = true
	# 
	  
	let rec aff_ele_et_rang ran elem = match (ran, elem) with                                      
	| (M,_) -> aff_ele elem
	| (P,F) -> "feu+lave"
	| (P,E) -> "eau+glace"
	| (P,T) -> "terre+bois"
	| (P,A) -> "aire+foudre"
	| (P,N) -> "aucun"
	| (U,I) -> "??"
	| (_,I) -> "?+"
	| (U,_) -> aff_ele_et_rang P elem ^ "?";;
	val aff_ele_et_rang : rang -> element -> string = <fun>
	# 
	let aff_perso_avec_rang p =
	  p.nom ^ "[" ^ aff_ele_et_rang p.ran p.ele ^ "]";;
	val aff_perso_avec_rang : personne -> string = <fun>
	let rec aff_arbre_with_rang = function
	| Feuille -> ""
	| Noeud (p1,Noeud(a,b,c),Noeud(d,e,f)) -> aff_perso_avec_rang p1 ^ "(" ^ aff_arbre_with_rang (Noeud(a,b,c)) ^
	  " , " ^ aff_arbre_with_rang (Noeud(d,e,f)) ^ ")"
	| Noeud (p1,Noeud(g,h,i),Feuille) -> aff_perso_avec_rang p1 ^ "(" ^ aff_arbre_with_rang (Noeud(g,h,i)) ^
	  " , "
	| Noeud (p1,Feuille,Noeud(k,l,m)) -> aff_perso_avec_rang p1 ^ aff_arbre_with_rang (Noeud(k,l,m)) ^
	  ")"
	| Noeud (p1,Feuille,Feuille) -> aff_perso_avec_rang p1;;
	val aff_arbre_with_rang : arbre -> string = <fun>
	# 
	let p1 = {nom = "a"; ele = I ; ran = P};;
	val p1 : personne = {nom = "a"; ele = I; ran = P}
	let p2 = {nom = "b"; ele = N ; ran = P};;
	val p2 : personne = {nom = "b"; ele = N; ran = P}
	let p3 = {nom = "c"; ele = A ; ran = M};;
	val p3 : personne = {nom = "c"; ele = A; ran = M}
	let p4 = {nom = "d"; ele = E ; ran = U};;
	val p4 : personne = {nom = "d"; ele = E; ran = U}
	let p5 = {nom = "e"; ele = I ; ran = U};;
	val p5 : personne = {nom = "e"; ele = I; ran = U}
	let p6 = {nom = "h"; ele = T ; ran = P};;
	val p6 : personne = {nom = "h"; ele = T; ran = P}
	let p7 = {nom = "i"; ele = I ; ran = M};;
	val p7 : personne = {nom = "i"; ele = I; ran = M}
	# 
	  
	  
	let f = Feuille;;
	val f : arbre = Feuille
	let a = Noeud(p6,f,f);;
	val a : arbre = Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille)
	let b = Noeud(p7,f,f);;
	val b : arbre = Noeud ({nom = "i"; ele = I; ran = M}, Feuille, Feuille)
	let c = Noeud(p3,f,f);;
	val c : arbre = Noeud ({nom = "c"; ele = A; ran = M}, Feuille, Feuille)
	let d = Noeud(p4,f,f);;
	val d : arbre = Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille)
	# 
	let arbre3 = Noeud(p1,Noeud(p2, d,Noeud(p5,a, b)), c);;

	val arbre3 : arbre =
	  Noeud ({nom = "a"; ele = I; ran = P},
	   Noeud ({nom = "b"; ele = N; ran = P},
	    Noeud ({nom = "d"; ele = E; ran = U}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = I; ran = U},
	     Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = I; ran = M}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = M}, Feuille, Feuille))
	aff_arbre_with_rang arbre3;;
	- : string =
	"a[?+](b[aucun](d[eau+glace?] , e[??](h[terre+bois] , i[?])) , c[air])"
	# 
	let verification_rang fils mere pere = match (fils.ran,mere.ran,pere.ran) with
	| (_, U, _) |(_,_,U) | (U,_,_)  -> true 
	| (P,_,_) -> true
	| (M,M,M) -> true
	| (_,_,_) -> false;;
	val verification_rang : personne -> personne -> personne -> bool = <fun>
	# 
	let rec verif_arbre_rang = function
	[] -> true
	| (a,b,c)::l -> if (verification_rang a b c) then true && verif_arbre_rang l else false;;
	val verif_arbre_rang : (personne * personne * personne) list -> bool = <fun>
	# 
	let p1 = {nom = "a"; ele = E ; ran = P};;
	val p1 : personne = {nom = "a"; ele = E; ran = P}
	let p2 = {nom = "b"; ele = N ; ran = P};;
	val p2 : personne = {nom = "b"; ele = N; ran = P}
	let p3 = {nom = "c"; ele = A ; ran = P};;
	val p3 : personne = {nom = "c"; ele = A; ran = P}
	let p4 = {nom = "d"; ele = E ; ran = M};;
	val p4 : personne = {nom = "d"; ele = E; ran = M}
	let p5 = {nom = "e"; ele = E ; ran = M};;
	val p5 : personne = {nom = "e"; ele = E; ran = M}
	let p8 = {nom = "f"; ele = A ; ran = M};;
	val p8 : personne = {nom = "f"; ele = A; ran = M}
	let p9 = {nom = "g"; ele = A ; ran = P};;
	val p9 : personne = {nom = "g"; ele = A; ran = P}
	let p6 = {nom = "h"; ele = T ; ran = P};;
	val p6 : personne = {nom = "h"; ele = T; ran = P}
	let p7 = {nom = "i"; ele = F ; ran = P};;
	val p7 : personne = {nom = "i"; ele = F; ran = P}
	# 
	let a = Noeud(p4,f,f);;
	val a : arbre = Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille)
	let b = Noeud(p6,f,f);;
	val b : arbre = Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille)
	let c = Noeud(p7,f,f);;
	val c : arbre = Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille)
	let d = Noeud(p8,f,f);;
	val d : arbre = Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille)
	let e = Noeud(p9,f,f);;
	val e : arbre = Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)
	# 
	  
	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = P},
	   Noeud ({nom = "b"; ele = N; ran = P},
	    Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = E; ran = M},
	     Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = P},
	    Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)))
	let list1 = empilement_Noeud arbre [];;
	verif_arbre_rang list1;;
	val list1 : (personne * personne * personne) list =
	  [({nom = "a"; ele = E; ran = P}, {nom = "b"; ele = N; ran = P},
	    {nom = "c"; ele = A; ran = P});
	   ({nom = "b"; ele = N; ran = P}, {nom = "d"; ele = E; ran = M},
	    {nom = "e"; ele = E; ran = M});
	   ({nom = "e"; ele = E; ran = M}, {nom = "h"; ele = T; ran = P},
	    {nom = "i"; ele = F; ran = P});
	   ({nom = "c"; ele = A; ran = P}, {nom = "f"; ele = A; ran = M},
	    {nom = "g"; ele = A; ran = P})]
	# - : bool = false
	# 
	let p1 = {nom = "a"; ele = E ; ran = P};;
	val p1 : personne = {nom = "a"; ele = E; ran = P}
	let p2 = {nom = "b"; ele = N ; ran = P};;
	val p2 : personne = {nom = "b"; ele = N; ran = P}
	let p3 = {nom = "c"; ele = A ; ran = P};;
	val p3 : personne = {nom = "c"; ele = A; ran = P}
	let p4 = {nom = "d"; ele = E ; ran = M};;
	val p4 : personne = {nom = "d"; ele = E; ran = M}
	let p5 = {nom = "e"; ele = E ; ran = P};;
	val p5 : personne = {nom = "e"; ele = E; ran = P}
	let p8 = {nom = "f"; ele = A ; ran = M};;
	val p8 : personne = {nom = "f"; ele = A; ran = M}
	let p9 = {nom = "g"; ele = A ; ran = P};;
	val p9 : personne = {nom = "g"; ele = A; ran = P}
	let p6 = {nom = "h"; ele = T ; ran = P};;
	val p6 : personne = {nom = "h"; ele = T; ran = P}
	let p7 = {nom = "i"; ele = F ; ran = P};;
	val p7 : personne = {nom = "i"; ele = F; ran = P}
	# 
	let a = Noeud(p4,f,f);;
	val a : arbre = Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille)
	let b = Noeud(p6,f,f);;
	val b : arbre = Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille)
	let c = Noeud(p7,f,f);;
	val c : arbre = Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille)
	let d = Noeud(p8,f,f);;
	val d : arbre = Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille)
	let e = Noeud(p9,f,f);;
	val e : arbre = Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)
	# 
	  
	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;
	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = P},
	   Noeud ({nom = "b"; ele = N; ran = P},
	    Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = E; ran = P},
	     Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = P},
	    Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)))
	let list2 = empilement_Noeud arbre [];;
	verif_arbre_rang list2;;
	val list2 : (personne * personne * personne) list =
	  [({nom = "a"; ele = E; ran = P}, {nom = "b"; ele = N; ran = P},
	    {nom = "c"; ele = A; ran = P});
	   ({nom = "b"; ele = N; ran = P}, {nom = "d"; ele = E; ran = M},
	    {nom = "e"; ele = E; ran = P});
	   ({nom = "e"; ele = E; ran = P}, {nom = "h"; ele = T; ran = P},
	    {nom = "i"; ele = F; ran = P});
	   ({nom = "c"; ele = A; ran = P}, {nom = "f"; ele = A; ran = M},
	    {nom = "g"; ele = A; ran = P})]
	# - : bool = true
	# 
	  
	let rec hauteur = function
	| Feuille -> 0
	| Noeud(_,g,d) -> 1 + (max (hauteur g) (hauteur d)) ;;
	val hauteur : arbre -> int = <fun>
	# 
	let rec par_niveau t l = match t with
	| Feuille -> []
	| Noeud(c, a, b) ->
	    if l = 1 then [c]
	    else par_niveau a (l - 1) @ par_niveau b (l - 1);;
	val par_niveau : arbre -> int -> personne list = <fun>
	# 
	let rec parcours_en_largeur arbre haut l =
	if haut = 0 then l
	else parcours_en_largeur arbre (haut-1) ((par_niveau arbre haut) :: l) ;;
	val parcours_en_largeur :
	  arbre -> int -> personne list list -> personne list list = <fun>
	# 
	  let li =parcours_en_largeur arbre hau [];;
	Error: Unbound value hau
	# 
	let lili = List.flatten li;;

	# t rec renverse l1 l2 = match l2 with
	  let lili = List.flatten li;;
	Error: Unbound value li
	| a :: tl -> renverse (a :: l1) tl;;
	val renverse : 'a list -> 'a list -> 'a list = <fun>
	# 
	let rec recherche_ancetre n= function
	[] -> ""
	|p::l -> if n=p.ele then p.nom else recherche_ancetre n l;;
	val recherche_ancetre : element -> personne list -> string = <fun>
	# 
	let p1 = {nom = "a"; ele = E ; ran = P};;
	val p1 : personne = {nom = "a"; ele = E; ran = P}
	let p2 = {nom = "b"; ele = N ; ran = P};;
	val p2 : personne = {nom = "b"; ele = N; ran = P}
	let p3 = {nom = "c"; ele = A ; ran = P};;
	val p3 : personne = {nom = "c"; ele = A; ran = P}
	let p4 = {nom = "d"; ele = E ; ran = M};;
	val p4 : personne = {nom = "d"; ele = E; ran = M}
	let p5 = {nom = "e"; ele = E ; ran = P};;
	val p5 : personne = {nom = "e"; ele = E; ran = P}
	let p8 = {nom = "f"; ele = A ; ran = M};;
	val p8 : personne = {nom = "f"; ele = A; ran = M}
	let p9 = {nom = "g"; ele = A ; ran = P};;
	val p9 : personne = {nom = "g"; ele = A; ran = P}
	let p6 = {nom = "h"; ele = T ; ran = P};;
	val p6 : personne = {nom = "h"; ele = T; ran = P}
	let p7 = {nom = "i"; ele = F ; ran = P};;
	val p7 : personne = {nom = "i"; ele = F; ran = P}
	# 
	let a = Noeud(p4,f,f);;
	val a : arbre = Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille)
	let b = Noeud(p6,f,f);;
	val b : arbre = Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille)
	let c = Noeud(p7,f,f);;
	val c : arbre = Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille)
	let d = Noeud(p8,f,f);;
	val d : arbre = Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille)
	let e = Noeud(p9,f,f);;
	val e : arbre = Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)
	# 
	  
	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = P},
	   Noeud ({nom = "b"; ele = N; ran = P},
	    Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = E; ran = P},
	     Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = P},
	    Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)))
	let hau = hauteur arbre;;
	val hau : int = 4
	let li =parcours_en_largeur arbre hau [];;

	val li : personne list list =
	  [[{nom = "a"; ele = E; ran = P}];
	   [{nom = "b"; ele = N; ran = P}; {nom = "c"; ele = A; ran = P}];
	   [{nom = "d"; ele = E; ran = M}; {nom = "e"; ele = E; ran = P};
	    {nom = "f"; ele = A; ran = M}; {nom = "g"; ele = A; ran = P}];
	   [{nom = "h"; ele = T; ran = P}; {nom = "i"; ele = F; ran = P}]]
	let lili = List.flatten li;;
	val lili : personne list =
	  [{nom = "a"; ele = E; ran = P}; {nom = "b"; ele = N; ran = P};
	   {nom = "c"; ele = A; ran = P}; {nom = "d"; ele = E; ran = M};
	   {nom = "e"; ele = E; ran = P}; {nom = "f"; ele = A; ran = M};
	   {nom = "g"; ele = A; ran = P}; {nom = "h"; ele = T; ran = P};
	   {nom = "i"; ele = F; ran = P}]
	# 
	let list_ancetre = renverse [] lili;;
	val list_ancetre : personne list =
	  [{nom = "i"; ele = F; ran = P}; {nom = "h"; ele = T; ran = P};
	   {nom = "g"; ele = A; ran = P}; {nom = "f"; ele = A; ran = M};
	   {nom = "e"; ele = E; ran = P}; {nom = "d"; ele = E; ran = M};
	   {nom = "c"; ele = A; ran = P}; {nom = "b"; ele = N; ran = P};
	   {nom = "a"; ele = E; ran = P}]
	recherche_ancetre E list_ancetre;;
	- : string = "e"
	recherche_ancetre I list_ancetre;;
	- : string = ""
	recherche_ancetre F list_ancetre;;
	- : string = "i"
	recherche_ancetre N list_ancetre;;
	- : string = "b"
	# 
	  
	let miroir a = match a with
	Feuille -> Feuille
	| Noeud(e,x,y)-> Noeud(e, y , x);;
	val miroir : arbre -> arbre = <fun>
	# 
	  
	let supp a = match a with 
	Feuille -> Feuille
	| Noeud (e,x,y) -> if e.ele = N then Noeud(e,f,f) else Noeud(e, x, y);;
	val supp : arbre -> arbre = <fun>
	# 
	  
	let modif_elem a = match  a with
	| Feuille -> Feuille
	| Noeud(e,x,y) -> if e.ele = A then Noeud({nom = e.nom ; ele = F ; ran = e.ran}, x, y) else  Noeud(e, x, y);;
	val modif_elem : arbre -> arbre = <fun>
	# 
	let modif_rang a = match a with 
	| Feuille -> Feuille
	| Noeud(e,x,y) -> if e.ran = P then Noeud({nom = e.nom ; ele = e.ele; ran = M}, x, y) else  Noeud({nom = e.nom ; ele = e.ele; ran = P}, x, y);;
	val modif_rang : arbre -> arbre = <fun>
	# 
	let rec ordre_sup f1 f2 a = match a with
	  | Feuille -> Feuille
	  | Noeud(e,x,y) -> f2 (f1 (Noeud(e, ordre_sup f2 f1 x , ordre_sup f2 f1 y)));;
	val ordre_sup : (arbre -> arbre) -> (arbre -> arbre) -> arbre -> arbre =
	  <fun>
	# 
	let p1 = {nom = "a"; ele = E ; ran = P};;
	val p1 : personne = {nom = "a"; ele = E; ran = P}
	let p2 = {nom = "b"; ele = N ; ran = P};;
	val p2 : personne = {nom = "b"; ele = N; ran = P}
	let p3 = {nom = "c"; ele = A ; ran = P};;
	val p3 : personne = {nom = "c"; ele = A; ran = P}
	let p4 = {nom = "d"; ele = E ; ran = M};;
	val p4 : personne = {nom = "d"; ele = E; ran = M}
	let p5 = {nom = "e"; ele = E ; ran = P};;
	val p5 : personne = {nom = "e"; ele = E; ran = P}
	let p8 = {nom = "f"; ele = A ; ran = M};;
	val p8 : personne = {nom = "f"; ele = A; ran = M}
	let p9 = {nom = "g"; ele = A ; ran = P};;
	val p9 : personne = {nom = "g"; ele = A; ran = P}
	let p6 = {nom = "h"; ele = T ; ran = P};;
	val p6 : personne = {nom = "h"; ele = T; ran = P}
	let p7 = {nom = "i"; ele = F ; ran = P};;
	val p7 : personne = {nom = "i"; ele = F; ran = P}
	# 
	let a = Noeud(p4,f,f);;
	val a : arbre = Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille)
	let b = Noeud(p6,f,f);;
	val b : arbre = Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille)
	let c = Noeud(p7,f,f);;
	val c : arbre = Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille)
	let d = Noeud(p8,f,f);;
	val d : arbre = Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille)
	let e = Noeud(p9,f,f);;
	val e : arbre = Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)
	# 
	  
	let arbre = Noeud(p1,Noeud(p2,a,Noeud(p5, b, c)),Noeud(p3, d , e));;

	val arbre : arbre =
	  Noeud ({nom = "a"; ele = E; ran = P},
	   Noeud ({nom = "b"; ele = N; ran = P},
	    Noeud ({nom = "d"; ele = E; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "e"; ele = E; ran = P},
	     Noeud ({nom = "h"; ele = T; ran = P}, Feuille, Feuille),
	     Noeud ({nom = "i"; ele = F; ran = P}, Feuille, Feuille))),
	   Noeud ({nom = "c"; ele = A; ran = P},
	    Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille),
	    Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille)))
	ordre_sup miroir supp arbre;;

	- : arbre =
	Noeud ({nom = "a"; ele = E; ran = P},
	 Noeud ({nom = "c"; ele = A; ran = P},
	  Noeud ({nom = "g"; ele = A; ran = P}, Feuille, Feuille),
	  Noeud ({nom = "f"; ele = A; ran = M}, Feuille, Feuille)),
	 Noeud ({nom = "b"; ele = N; ran = P}, Feuille, Feuille))
	#   ordre_sup modif_rang modif_elem arbre;;
	- : arbre =
	Noeud ({nom = "a"; ele = E; ran = M},
	 Noeud ({nom = "b"; ele = N; ran = M},
	  Noeud ({nom = "d"; ele = E; ran = P}, Feuille, Feuille),
	  Noeud ({nom = "e"; ele = E; ran = M},
	   Noeud ({nom = "h"; ele = T; ran = M}, Feuille, Feuille),
	   Noeud ({nom = "i"; ele = F; ran = M}, Feuille, Feuille))),
	 Noeud ({nom = "c"; ele = F; ran = M},
	  Noeud ({nom = "f"; ele = F; ran = P}, Feuille, Feuille),
	  Noeud ({nom = "g"; ele = F; ran = M}, Feuille, Feuille)))
	# 
*)

